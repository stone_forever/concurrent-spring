package com.flowyun.cornerstone.basic.concurrent.exclusive.annotations;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Exclusive {
    /*
    * 锁名称
    * */
    String value() default "";
    /*
    * 如果希望使用不同的锁管理器
    * */
    String exclusiveRepository() default "";
    /*
     * 加锁失败时，提示信息
     * */
    String messageWhenLockFailed() default "lock failed";
    /*
    * 加锁方式
    * */
    LockMethod lockMethod() default LockMethod.TRY;
    /*
    * 仅在try方式下可用
    * 争取锁定时间，值等于-1时：
    *  在try模式下，锁定失败立刻返回
    * */
    int timeout() default -1;
    /*
    * 锁定时间计时单位
    * */
    TimeUnit timeUnit() default TimeUnit.MILLISECONDS;
}
