package com.flowyun.cornerstone.basic.concurrent.redission;

import org.redisson.config.Config;
import org.redisson.config.ConfigSupport;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;

public class RedissonShardingConfig extends Config {
    //分片权重，影响自动分片时选中的比例
    private int weight = 1;

    public RedissonShardingConfig() {
    }

    public RedissonShardingConfig(RedissonShardingConfig oldConf) {
        super(oldConf);
        setWeight(oldConf.weight);
    }

    public int getWeight() {
        return weight;
    }

    void setWeight(int weight) {
        if(weight>=1){
            this.weight = weight;
        }
    }

    /**
     * Read config object stored in YAML format from <code>String</code>
     *
     * @param content of config
     * @return config
     * @throws IOException error
     */
    public static RedissonShardingConfig fromYAML(String content) throws IOException {
        ConfigSupport support = new ConfigSupport();
        return support.fromYAML(content, RedissonShardingConfig.class);
    }

    /**
     * Read config object stored in YAML format from <code>InputStream</code>
     *
     * @param inputStream object
     * @return config
     * @throws IOException error
     */
    public static RedissonShardingConfig fromYAML(InputStream inputStream) throws IOException {
        ConfigSupport support = new ConfigSupport();
        return support.fromYAML(inputStream, RedissonShardingConfig.class);
    }

    /**
     * Read config object stored in YAML format from <code>File</code>
     *
     * @param file object
     * @return config
     * @throws IOException error
     */
    public static RedissonShardingConfig fromYAML(File file) throws IOException {
        return fromYAML(file, null);
    }

    public static RedissonShardingConfig fromYAML(File file, ClassLoader classLoader) throws IOException {
        ConfigSupport support = new ConfigSupport();
        return support.fromYAML(file, RedissonShardingConfig.class, classLoader);
    }

    /**
     * Read config object stored in YAML format from <code>URL</code>
     *
     * @param url object
     * @return config
     * @throws IOException error
     */
    public static RedissonShardingConfig fromYAML(URL url) throws IOException {
        ConfigSupport support = new ConfigSupport();
        return support.fromYAML(url, RedissonShardingConfig.class);
    }

    /**
     * Read config object stored in YAML format from <code>Reader</code>
     *
     * @param reader object
     * @return config
     * @throws IOException error
     */
    public static RedissonShardingConfig fromYAML(Reader reader) throws IOException {
        ConfigSupport support = new ConfigSupport();
        return support.fromYAML(reader, RedissonShardingConfig.class);
    }

    /**
     * Convert current configuration to YAML format
     *
     * @return config in yaml format
     * @throws IOException error
     */
    public String toYAML() throws IOException {
        ConfigSupport support = new ConfigSupport();
        return support.toYAML(this);
    }

}
