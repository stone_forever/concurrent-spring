package com.flowyun.cornerstone.basic.concurrent.exclusive.annotations;

import com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor.ExclusiveAttributeSource;
import com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor.ExclusivePointcut;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractBeanFactoryPointcutAdvisor;
import org.springframework.util.Assert;

public class BeanFactoryExclusiveLockAdvisor extends AbstractBeanFactoryPointcutAdvisor {
    private ExclusivePointcut pointcut;

    public void setExclusiveAttributeSource(ExclusiveAttributeSource exclusiveAttributeSource) {
        Assert.notNull(exclusiveAttributeSource,"exclusive attribute source is null");
        this.pointcut = new ExclusivePointcut(exclusiveAttributeSource);
    }

    @Override
    public Pointcut getPointcut() {
        return pointcut;
    }

    public void setClassFilter(ClassFilter classFilter) {
        if(pointcut!=null){
            this.pointcut.setClassFilter(classFilter);
        }
    }
}
