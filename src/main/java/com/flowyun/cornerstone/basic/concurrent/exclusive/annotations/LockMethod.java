package com.flowyun.cornerstone.basic.concurrent.exclusive.annotations;

public enum LockMethod {
    TRY,
    WAITING
}
