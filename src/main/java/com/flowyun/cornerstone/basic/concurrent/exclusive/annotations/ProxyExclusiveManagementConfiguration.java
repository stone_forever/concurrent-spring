package com.flowyun.cornerstone.basic.concurrent.exclusive.annotations;

import com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor.ExclusiveAttributeSource;
import com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor.ExclusiveInterceptor;
import com.flowyun.cornerstone.basic.concurrent.exclusive.repository.ExclusiveRepository;
import com.flowyun.cornerstone.basic.concurrent.exclusive.repository.LocalExclusiveRepository;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.annotation.Role;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.Nullable;

@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ProxyExclusiveManagementConfiguration  implements ImportAware {

    @Nullable
    protected AnnotationAttributes enableEx;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {

        this.enableEx = AnnotationAttributes.fromMap(
                importMetadata.getAnnotationAttributes(EnableExclusive.class.getName(), false));
        if (this.enableEx == null) {
            throw new IllegalArgumentException(
                    "@EnableExclusive is not present on importing class " + importMetadata.getClassName());
        }

    }

    @Bean(name = "com.flowyun.cornerstone.econage-exclusive-lock-advisor")
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public BeanFactoryExclusiveLockAdvisor econageExclusiveAdvisor(
            ExclusiveAttributeSource exclusiveAttributeSource,
            ExclusiveInterceptor exclusiveInterceptor
    ) {
        BeanFactoryExclusiveLockAdvisor advisor = new BeanFactoryExclusiveLockAdvisor();
        advisor.setExclusiveAttributeSource(exclusiveAttributeSource);
        advisor.setAdvice(exclusiveInterceptor);
        if (this.enableEx != null) {
            advisor.setOrder(this.enableEx.<Integer>getNumber("order"));
        }
        return advisor;
    }

    @Bean(name = "com.flowyun.cornerstone.econage-exclusive-attribute-source")
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public ExclusiveAttributeSource econageExclusiveAttributeSource(){
        return new ExclusiveAttributeSource();
    }

    @Bean(name = "com.flowyun.cornerstone.econage-exclusive-interceptor")
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public ExclusiveInterceptor econageExclusiveInterceptor(
            ExclusiveAttributeSource exclusiveAttributeSource,
            ObjectProvider<ExclusiveRepository> exclusiveRepositoryObjectProvider
    ){
        ExclusiveInterceptor exclusiveInterceptor =  new ExclusiveInterceptor();
        exclusiveInterceptor.setExclusiveAttributeSource(exclusiveAttributeSource);
        exclusiveInterceptor.setExclusiveRepository(
                exclusiveRepositoryObjectProvider.getIfAvailable(LocalExclusiveRepository::new)
        );
        return exclusiveInterceptor;
    }

}
