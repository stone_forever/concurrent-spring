package com.flowyun.cornerstone.basic.concurrent.executor;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class TaskQueue extends LinkedBlockingQueue<Runnable> {

    private static final long serialVersionUID = 1L;

    private volatile ThreadPoolExecutor parent = null;


    public TaskQueue() {
        super();
    }

    public TaskQueue(int capacity) {
        super(capacity);
    }

    public TaskQueue(Collection<? extends Runnable> c) {
        super(c);
    }

    public void setParent(ThreadPoolExecutor tp) {
        parent = tp;
    }


    public boolean force(Runnable o) {
        if ( parent==null || parent.isShutdown() ) throw new RejectedExecutionException("Executor not running, can't force a command into the queue");
        return super.offer(o);
    }

    public boolean force(Runnable o, long timeout, TimeUnit unit) throws InterruptedException {
        if ( parent==null || parent.isShutdown() ) throw new RejectedExecutionException("Executor not running, can't force a command into the queue");
        return super.offer(o,timeout,unit);
    }


    @Override
    public boolean offer(Runnable o) {
        //没线程池。。。直接使用原始逻辑
        if (parent==null) return super.offer(o);
        //线程池已满，任务放入队列
        if (parent.getPoolSize() == parent.getMaximumPoolSize()) return super.offer(o);
        //有线程空闲
        if (parent.getSubmittedCount()<(parent.getPoolSize())) return super.offer(o);
        //如果线程数未达到最大，返回false，使线程池扩大
        if (parent.getPoolSize()<parent.getMaximumPoolSize()) return false;

        return super.offer(o);
    }



}
