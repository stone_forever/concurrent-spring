package com.flowyun.cornerstone.basic.concurrent.redission;

import com.google.common.collect.Maps;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.redisson.Redisson;
import org.redisson.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;

/*
* 简易分片管理器器，用一颗红黑树统一管理redisson客户端
* */
public class RedissonShardingManager implements InitializingBean, DisposableBean {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final HashFunction HASH_FUNCTION = Hashing.murmur3_128(0x1234ABCD);
    public static Long parseHashCode(String key){
        return HASH_FUNCTION.newHasher()
                .putString(key, StandardCharsets.UTF_8)
                .hash()
                .asLong();
    }

    private final TreeMap<Long, RedissonClient> redissonShardingMap = Maps.newTreeMap();
    private final List<RedissonShardingConfig> shardingConfigs;

    public RedissonShardingManager(List<RedissonShardingConfig> shardingConfigs) {
        this.shardingConfigs = shardingConfigs;
    }

    /*
    * 区域id，如果map、锁对象，可以是一个具体的key值（锁名、map中的key值）
    * */
    public RedissonClient findSharding(String region){
        SortedMap<Long,RedissonClient> tail = redissonShardingMap.tailMap( parseHashCode(region) );
        if (tail.isEmpty()) {
            return redissonShardingMap.get(redissonShardingMap.firstKey());
        }
        return tail.get(tail.firstKey());
    }

    /*
    * ------------------------------------部分高频快捷方法
    * */

    public Lock getLock(String name){
        Assert.hasText(name,"name is empty!");
        return findSharding(name).getLock(name);
    }
    /*
    * map可以先有region决定分片位置，再由mapName指定具体的map对象
    * */
    public <K,V> RLocalCachedMap<K,V> getLocalCachedMap(String region, String mapName){
        Assert.hasText(region,"region is empty!");
        Assert.hasText(mapName,"mapName is empty!");

        LocalCachedMapOptions<K, V> options = LocalCachedMapOptions.<K, V>defaults()
                .reconnectionStrategy(LocalCachedMapOptions.ReconnectionStrategy.CLEAR)
                .syncStrategy(LocalCachedMapOptions.SyncStrategy.INVALIDATE);

        return findSharding(region).getLocalCachedMap(mapName,options);
    }

    public <T> RBucket<T> getBucket(String name){
        Assert.hasText(name,"name is empty!");
        return findSharding(name).getBucket(name);
    }

    public RRateLimiter getRateLimiter(String name){
        Assert.hasText(name,"name is empty!");
        return findSharding(name).getRateLimiter(name);
    }

    public RTopic getTopic(String name){
        Assert.hasText(name,"name is empty!");
        return findSharding(name).getTopic(name);
    }
    /*
     * ------------------------------------部分高频快捷方法------------------------------------
     * */

    @Override
    public void afterPropertiesSet() throws Exception {

        if(CollectionUtils.isEmpty(shardingConfigs)){
            throw new IllegalStateException("shardingConfigs is required.");
        }else if(!CollectionUtils.isEmpty(redissonShardingMap)){
            return;
        }

        int shardingIdx = 1;
        for(RedissonShardingConfig redissonConfig : shardingConfigs ){
            String shardingBaseName = "sharding-" + shardingIdx + "-slot-";
            RedissonClient redisson = Redisson.create(redissonConfig);

            for (int n = 0,l = 160*redissonConfig.getWeight(); n < l; n++) {
                redissonShardingMap.put(
                        parseHashCode(shardingBaseName + n),
                        redisson
                );
            }
            shardingIdx++;
        }

    }

    @Override
    public void destroy() throws Exception {
        for(RedissonClient client : redissonShardingMap.values() ){
            try{
                client.shutdown();
            }catch(Throwable t){
                logger.info(t.getMessage(),t);
            }
        }
    }

}
