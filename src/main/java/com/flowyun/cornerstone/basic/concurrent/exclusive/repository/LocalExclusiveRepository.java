package com.flowyun.cornerstone.basic.concurrent.exclusive.repository;

import org.springframework.util.StringUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

/*
* 本地锁管理器，默认方式
* 如果需要分布式方案，可以配合redission编写一个
* */
public class LocalExclusiveRepository implements ExclusiveRepository {

    private final boolean lockFair;
    private final Function<String,Lock> lockCreator;
    private final ConcurrentHashMap<String,Lock> lockHolder = new ConcurrentHashMap<>(1024);

    public LocalExclusiveRepository() {
        this(false);
    }

    public LocalExclusiveRepository(final boolean lockFair) {
        this.lockFair = lockFair;
        this.lockCreator = ln-> new ReentrantLock(lockFair);
    }

    public boolean isLockFair() {
        return lockFair;
    }

    @Override
    public Lock getLock(String lockName) {
        if(StringUtils.isEmpty(lockName)){
            throw new IllegalArgumentException("lock name is null");
        }
        return lockHolder.computeIfAbsent(lockName,lockCreator);
    }
}
