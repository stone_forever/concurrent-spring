package com.flowyun.cornerstone.basic.concurrent.exclusive.repository;

import java.util.concurrent.locks.Lock;

public interface ExclusiveRepository {

    Lock getLock(String lockName);

    //自定义实现可以替换为国际化内容，默认直接返回
    default String resolveMessageWhenLockFail(String msg){
        return msg;
    }
}
