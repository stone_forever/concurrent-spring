package com.flowyun.cornerstone.basic.concurrent.exclusive;

public class ExclusiveLockException extends RuntimeException {

    public ExclusiveLockException() {
        super();
    }
    public ExclusiveLockException(String message) {
        super(message);
    }
    public ExclusiveLockException(String message, Throwable cause) {
        super(message, cause);
    }
    public ExclusiveLockException(Throwable cause) {
        super(cause);
    }

}
