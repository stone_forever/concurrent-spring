package com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor;

import org.springframework.aop.support.StaticMethodMatcherPointcut;

import java.io.Serializable;
import java.lang.reflect.Method;

public class ExclusivePointcut extends StaticMethodMatcherPointcut implements Serializable {

    private ExclusiveAttributeSource exclusiveAttributeSource;

    public ExclusivePointcut(
            ExclusiveAttributeSource exclusiveAttributeSource
    ) {
        this.exclusiveAttributeSource = exclusiveAttributeSource;
        setClassFilter(exclusiveAttributeSource::isCandidateClass);
    }

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        return exclusiveAttributeSource.getExclusiveAttribute(method, targetClass) != null;
    }

}
