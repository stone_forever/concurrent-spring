package com.flowyun.cornerstone.basic.concurrent.executor;

import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.scheduling.SchedulingTaskExecutor;
import org.springframework.scheduling.concurrent.ExecutorConfigurationSupport;
import org.springframework.util.Assert;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureTask;

import java.util.concurrent.*;

/*
* 利用改造过的ThreadPoolExecutor，配合TaskQueue，更改了线程池默认的行为
* 当有新任务是，会尽可能的创建线程，直到线程池满
* 线程池达到最大数后，将任务放入队列
* 如果任务满，线程池达到最大数线程数，则会尝试在加一次队列
* 如果还是失败，则抛出拒绝服务异常
* */
public class ThreadFirstExecutor extends ExecutorConfigurationSupport
        implements AsyncListenableTaskExecutor, SchedulingTaskExecutor {
    //public static final Logger logger = LoggerFactory.getLogger(StandardThreadExecutor.class);

    protected int maxThreads = 200;
    protected int minSpareThreads = 25;
    protected int maxIdleTime = 60000;
    protected boolean prestartminSpareThreads = false;
    protected int maxQueueSize = Integer.MAX_VALUE;
    private TaskQueue taskqueue = null;
    private ThreadPoolExecutor threadPoolExecutor;

    public ThreadFirstExecutor() {
        setThreadPriority(Thread.NORM_PRIORITY);
        //web环境下，线程全部是守护线程
        setDaemon(true);
        setThreadNamePrefix("econage-exec-");
        setThreadGroupName("econage-group-");
    }

    @Override
    public void shutdown() {
        super.shutdown();
        threadPoolExecutor = null;
        taskqueue = null;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public int getMinSpareThreads() {
        return minSpareThreads;
    }

    public void setMinSpareThreads(int minSpareThreads) {
        this.minSpareThreads = minSpareThreads;
    }

    public int getMaxIdleTime() {
        return maxIdleTime;
    }

    public void setMaxIdleTime(int maxIdleTime) {
        this.maxIdleTime = maxIdleTime;
    }

    public boolean isPrestartminSpareThreads() {
        return prestartminSpareThreads;
    }

    public void setPrestartminSpareThreads(boolean prestartminSpareThreads) {
        this.prestartminSpareThreads = prestartminSpareThreads;
    }

    public int getMaxQueueSize() {
        return maxQueueSize;
    }

    public void setMaxQueueSize(int maxQueueSize) {
        this.maxQueueSize = maxQueueSize;
    }

    @Override
    public Thread createThread(Runnable runnable) {
        Thread thread = super.createThread(runnable);
        //在新建一个线程后，重设线程上下文类加载器，方便web环境使用
        thread.setContextClassLoader(getClass().getClassLoader());
        return thread;
    }

    @Override
    protected ExecutorService initializeExecutor(ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {

        this.taskqueue = new TaskQueue(maxQueueSize);
        this.threadPoolExecutor = new ThreadPoolExecutor(
                getMinSpareThreads(), getMaxThreads(),
                maxIdleTime, TimeUnit.MILLISECONDS,
                taskqueue, this
        );
        if (prestartminSpareThreads) {
            threadPoolExecutor.prestartAllCoreThreads();
        }
        this.taskqueue.setParent(threadPoolExecutor);
        return threadPoolExecutor;
    }

    @Override
    public ListenableFuture<?> submitListenable(Runnable task) {
        ExecutorService executor = getThreadPoolExecutor();
        try {
            ListenableFutureTask<Object> future = new ListenableFutureTask<Object>(task, null);
            executor.execute(future);
            return future;
        }
        catch (RejectedExecutionException ex) {
            throw new TaskRejectedException("Executor [" + executor + "] did not accept task: " + task, ex);
        }
    }

    @Override
    public <T> ListenableFuture<T> submitListenable(Callable<T> task) {
        ExecutorService executor = getThreadPoolExecutor();
        try {
            ListenableFutureTask<T> future = new ListenableFutureTask<T>(task);
            executor.execute(future);
            return future;
        }
        catch (RejectedExecutionException ex) {
            throw new TaskRejectedException("Executor [" + executor + "] did not accept task: " + task, ex);
        }
    }

    @Override
    public boolean prefersShortLivedTasks() {
        return true;
    }

    @Override
    public Future<?> submit(Runnable task) {
        ExecutorService executor = getThreadPoolExecutor();
        try {
            return executor.submit(task);
        }
        catch (RejectedExecutionException ex) {
            throw new TaskRejectedException("Executor [" + executor + "] did not accept task: " + task, ex);
        }
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        ExecutorService executor = getThreadPoolExecutor();
        try {
            return executor.submit(task);
        }
        catch (RejectedExecutionException ex) {
            throw new TaskRejectedException("Executor [" + executor + "] did not accept task: " + task, ex);
        }
    }

    @Override
    public void execute(Runnable task) {
        ThreadPoolExecutor executor = getThreadPoolExecutor();
        try {
            executor.execute(task);
        } catch (RejectedExecutionException ex) {
            if ( !( (TaskQueue) executor.getQueue()).force(task) ) throw new TaskRejectedException("Work queue full.");
        }
    }
    @Override
    public void execute(Runnable task, long startTimeout) {
        execute(task);
    }

    public void execute(Runnable task, long timeout, TimeUnit unit) {
        getThreadPoolExecutor().execute(task,timeout,unit);
    }


    public ThreadPoolExecutor getThreadPoolExecutor() throws IllegalStateException {
        Assert.state(this.threadPoolExecutor != null, "ThreadPoolTaskExecutor not initialized");
        return this.threadPoolExecutor;
    }
}
