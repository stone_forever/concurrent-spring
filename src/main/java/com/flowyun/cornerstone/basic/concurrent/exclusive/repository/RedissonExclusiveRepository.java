package com.flowyun.cornerstone.basic.concurrent.exclusive.repository;

import com.flowyun.cornerstone.basic.concurrent.redission.RedissonShardingManager;

import java.util.concurrent.locks.Lock;

public class RedissonExclusiveRepository implements ExclusiveRepository {
    private final RedissonShardingManager redissonShardingManager;

    public RedissonExclusiveRepository(RedissonShardingManager redissonShardingManager) {
        this.redissonShardingManager = redissonShardingManager;
    }

    @Override
    public Lock getLock(String lockName) {
        return redissonShardingManager.getLock(lockName);
    }
}
