package com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor;

import com.flowyun.cornerstone.basic.concurrent.exclusive.annotations.LockMethod;

import java.util.concurrent.TimeUnit;

public class ExclusiveAttribute {

    private final String lockName;
    private final String exclusiveRepository;
    private final String messageWhenLockFailed;
    private final LockMethod lockMethod ;
    private final int timeout;
    private final TimeUnit timeUnit;

    public ExclusiveAttribute(
            String lockName, String exclusiveRepository,
            String messageWhenLockFailed, LockMethod lockMethod,
            int timeout, TimeUnit timeUnit
    ) {
        this.lockName = lockName;
        this.exclusiveRepository = exclusiveRepository;
        this.messageWhenLockFailed = messageWhenLockFailed;
        this.lockMethod = lockMethod;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    private String descriptor;

    public String getLockName() {
        return lockName;
    }

    public String getExclusiveRepository() {
        return exclusiveRepository;
    }

    public String getMessageWhenLockFailed() {
        return messageWhenLockFailed;
    }

    public LockMethod getLockMethod() {
        return lockMethod;
    }

    public int getTimeout() {
        return timeout;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public String getDescriptor() {
        return descriptor;
    }

    void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public String toString() {
        return getDefinitionDescription().toString();
    }

    protected final StringBuilder getDefinitionDescription() {
        StringBuilder result = new StringBuilder();
        result.append(lockName);
        result.append(',');
        result.append(messageWhenLockFailed);
        result.append(',');
        result.append(exclusiveRepository);
        result.append(',');
        result.append(lockMethod);

        if (this.timeout != -1) {
            result.append(',');
            result.append(this.timeout);
        }
        result.append(',');
        result.append(timeUnit);
        return result;
    }

}
