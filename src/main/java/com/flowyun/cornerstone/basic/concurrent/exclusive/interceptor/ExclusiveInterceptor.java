package com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor;

import com.flowyun.cornerstone.basic.concurrent.exclusive.ExclusiveLockException;
import com.flowyun.cornerstone.basic.concurrent.exclusive.annotations.LockMethod;
import com.flowyun.cornerstone.basic.concurrent.exclusive.repository.ExclusiveRepository;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ConcurrentReferenceHashMap;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;

public class ExclusiveInterceptor  implements BeanFactoryAware, InitializingBean, MethodInterceptor, Serializable {

    private static final Object DEFAULT_EXCLUSIVE_REPOSITORY_KEY = new Object();

    protected final Log logger = LogFactory.getLog(getClass());

    @Nullable
    private String exclusiveRepositoryBeanName;

    @Nullable
    private ExclusiveRepository exclusiveRepository;

    @Nullable
    private ExclusiveAttributeSource exclusiveAttributeSource;

    @Nullable
    private BeanFactory beanFactory;

    private final ConcurrentMap<Object, ExclusiveRepository> exclusiveRepositoryCache =
            new ConcurrentReferenceHashMap<>(4);

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Class<?> targetClass = (invocation.getThis() != null ? AopUtils.getTargetClass(invocation.getThis()) : null);

        return invokeWithinTransaction(invocation.getMethod(), targetClass, invocation);
    }

    @Nullable
    public String getExclusiveRepositoryBeanName() {
        return exclusiveRepositoryBeanName;
    }

    public void setExclusiveRepositoryBeanName(@Nullable String exclusiveRepositoryBeanName) {
        this.exclusiveRepositoryBeanName = exclusiveRepositoryBeanName;
    }

    public void setExclusiveRepository(@Nullable ExclusiveRepository exclusiveRepository) {
        this.exclusiveRepository = exclusiveRepository;
    }

    public void setExclusiveAttributeSource(@Nullable ExclusiveAttributeSource exclusiveAttributeSource) {
        this.exclusiveAttributeSource = exclusiveAttributeSource;
    }

    @Nullable
    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    public ExclusiveAttributeSource getExclusiveAttributeSource() {
        return exclusiveAttributeSource;
    }

    public ExclusiveRepository getExclusiveRepository() {
        return exclusiveRepository;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (getExclusiveRepository() == null && this.beanFactory == null) {
            throw new IllegalStateException(
                    "Set the 'exclusiveRepository' property or make sure to run within a BeanFactory " +
                            "containing a ExclusiveRepository bean!");
        }
        if (getExclusiveAttributeSource() == null) {
            throw new IllegalStateException(
                    "Either 'exclusiveAttributeSource' or 'exclusiveAttributes' is required: " +
                            "If there are no exclusive methods, then don't use a exclusive aspect.");
        }
    }


    @Nullable
    protected Object invokeWithinTransaction(
            Method method, @Nullable Class<?> targetClass,
            final MethodInvocation invocation
    ) throws Throwable {

        ExclusiveAttributeSource eas = getExclusiveAttributeSource();
        final ExclusiveAttribute exAttr = (eas != null ? eas.getExclusiveAttribute(method, targetClass) : null);
        final ExclusiveRepository em = determineExclusiveRepository(exAttr);
        final String joinpointIdentification = methodIdentification(method, targetClass, exAttr);

        ExclusiveLockInfo txInfo = createExclusiveIfNecessary(em, exAttr, joinpointIdentification);
        try{
            if (txInfo != null){
                txInfo.lock();
            }
            Object retVal = invocation.proceed();

            //完成打印日志
            if (txInfo != null && txInfo.getLock() != null) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Completing exclusive for [" + txInfo.getJoinpointIdentification() + "]");
                }
            }

            return retVal;
        } catch (Throwable ex) {
            if (txInfo != null && txInfo.getLock() != null) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Completing exclusive for [" + txInfo.getJoinpointIdentification() + "] after exception: " + ex);
                }
            }
            throw ex;
        }finally{
            if (txInfo != null) {
                txInfo.unLock();
            }
        }

    }


    @Nullable
    protected ExclusiveRepository determineExclusiveRepository(@Nullable ExclusiveAttribute txAttr) {
        // Do not attempt to lookup tx Repository if no tx attributes are set
        if (txAttr == null || this.beanFactory == null) {
            return getExclusiveRepository();
        }

        String qualifier = txAttr.getExclusiveRepository();
        if (StringUtils.hasText(qualifier)) {
            return determineQualifiedExclusiveRepository(this.beanFactory, qualifier);
        }
        else if (StringUtils.hasText(this.exclusiveRepositoryBeanName)) {
            return determineQualifiedExclusiveRepository(this.beanFactory, this.exclusiveRepositoryBeanName);
        }
        else {
            ExclusiveRepository defaultTransactionRepository = getExclusiveRepository();
            if (defaultTransactionRepository == null) {
                defaultTransactionRepository = this.exclusiveRepositoryCache.get(DEFAULT_EXCLUSIVE_REPOSITORY_KEY);
                if (defaultTransactionRepository == null) {
                    defaultTransactionRepository = this.beanFactory.getBean(ExclusiveRepository.class);
                    this.exclusiveRepositoryCache.putIfAbsent(
                            DEFAULT_EXCLUSIVE_REPOSITORY_KEY, defaultTransactionRepository);
                }
            }
            return defaultTransactionRepository;
        }
    }

    private ExclusiveRepository determineQualifiedExclusiveRepository(BeanFactory beanFactory, String qualifier) {
        ExclusiveRepository txRepository = this.exclusiveRepositoryCache.get(qualifier);
        if (txRepository == null) {
            txRepository = BeanFactoryAnnotationUtils.qualifiedBeanOfType(
                    beanFactory, ExclusiveRepository.class, qualifier);
            this.exclusiveRepositoryCache.putIfAbsent(qualifier, txRepository);
        }
        return txRepository;
    }

    private String methodIdentification(
            Method method, @Nullable Class<?> targetClass,
            @Nullable ExclusiveAttribute txAttr
    ) {
        String methodIdentification = null;
        if (txAttr !=null) {
            methodIdentification =  txAttr.getDescriptor();
        }
        if (methodIdentification == null) {
            methodIdentification = ClassUtils.getQualifiedMethodName(method, targetClass);
        }
        return methodIdentification;
    }

    protected ExclusiveLockInfo createExclusiveIfNecessary(
            @Nullable ExclusiveRepository tm,
            @Nullable ExclusiveAttribute exAttr,
            final String joinpointIdentification
    ) {

        Lock lock = null;
        if (exAttr != null) {
            if (tm != null) {
                lock = tm.getLock(exAttr.getLockName());
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Skipping exclusive joinpoint [" + joinpointIdentification +
                            "] because no exclusive Repository has been configured");
                }
            }
        }
        return prepareExclusiveInfo(tm, exAttr, joinpointIdentification, lock);
    }

    /**
     * Prepare a ExclusiveLockInfo for the given attribute
     * @param exAttr the ExclusiveAttribute (may be {@code null})
     * @param joinpointIdentification the fully qualified method name
     * (used for monitoring and logging purposes)
     * @param lock the lock for the current method
     * @return the prepared ExclusiveLockInfo object
     */
    protected ExclusiveLockInfo prepareExclusiveInfo(@Nullable ExclusiveRepository em,
                                                     @Nullable ExclusiveAttribute exAttr, String joinpointIdentification,
                                                     @Nullable Lock lock) {

        ExclusiveLockInfo txInfo = new ExclusiveLockInfo(em, exAttr, joinpointIdentification);
        if (exAttr != null) {
            // We need a exclusive for this method...
            if (logger.isTraceEnabled()) {
                logger.trace("Getting exclusive for [" + txInfo.getJoinpointIdentification() + "]");
            }
            // The exclusive Repository will flag an error if an incompatible tx already exists.
            txInfo.newLock(lock);
        }
        else {
            if (logger.isTraceEnabled()) {
                logger.trace("No need to create exclusive for [" + joinpointIdentification +
                        "]: This method is not exclusive.");
            }
        }

        return txInfo;
    }


    protected static final class ExclusiveLockInfo{

        @Nullable
        private final ExclusiveRepository exclusiveRepository;

        @Nullable
        private final ExclusiveAttribute exclusiveAttribute;

        private final String joinpointIdentification;

        private Lock lock;

        private boolean lockSuccess;

        public ExclusiveLockInfo(@Nullable ExclusiveRepository exclusiveRepository,
                               @Nullable ExclusiveAttribute exclusiveAttribute, String joinpointIdentification) {

            this.exclusiveRepository = exclusiveRepository;
            this.exclusiveAttribute = exclusiveAttribute;
            this.joinpointIdentification = joinpointIdentification;
        }

        public ExclusiveRepository getExclusiveRepository() {
            Assert.state(this.exclusiveRepository != null, "No ExclusiveRepository set");
            return this.exclusiveRepository;
        }

        @Nullable
        public ExclusiveAttribute getExclusiveAttribute() {
            return this.exclusiveAttribute;
        }

        public String getJoinpointIdentification() {
            return this.joinpointIdentification;
        }

        public void newLock(@Nullable Lock status) {
            this.lock = status;
        }

        @Nullable
        public Lock getLock() {
            return this.lock;
        }

        @Override
        public String toString() {
            return (this.exclusiveAttribute != null ? this.exclusiveAttribute.toString() : "No exclusive");
        }

        //根据配置信息，做加锁操作
        public void lock(){
            try{
                doLock();
            }catch(ExclusiveLockException ex){
                throw ex;
            }catch(Throwable ex){
                throw new ExclusiveLockException(ex);
            }
        }

        private void doLock() throws InterruptedException {
            if(lock==null||exclusiveAttribute==null){
                return;
            }
            if(exclusiveAttribute.getLockMethod()== LockMethod.TRY){
                if(exclusiveAttribute.getTimeout()<=0){
                    lockSuccess = lock.tryLock();
                }else{
                    lockSuccess = lock.tryLock(exclusiveAttribute.getTimeout(),exclusiveAttribute.getTimeUnit());
                }
                if(!lockSuccess){
                    throw new ExclusiveLockException(
                            exclusiveRepository
                                    .resolveMessageWhenLockFail(exclusiveAttribute.getMessageWhenLockFailed())
                    );
                }
            }else{
                lock.lockInterruptibly();
                lockSuccess = true;
            }
        }

        public void unLock() {
            if(lock!=null&&lockSuccess){
                lock.unlock();
            }
        }
    }
}
