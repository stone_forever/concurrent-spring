package com.flowyun.cornerstone.basic.concurrent.exclusive.interceptor;

import com.flowyun.cornerstone.basic.concurrent.exclusive.annotations.Exclusive;
import com.flowyun.cornerstone.basic.concurrent.exclusive.annotations.LockMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.MethodClassKey;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class ExclusiveAttributeSource {
    protected final Log logger = LogFactory.getLog(getClass());
    private final boolean publicMethodsOnly;

    public ExclusiveAttributeSource() {
        this(true);
    }

    public ExclusiveAttributeSource(boolean publicMethodsOnly) {
        this.publicMethodsOnly = publicMethodsOnly;
    }

    public boolean isCandidateClass(Class<?> targetClass) {
        return AnnotationUtils.isCandidateClass(targetClass, Exclusive.class);
    }

    private static final ExclusiveAttribute NULL_EXCLUSIVE_ATTRIBUTE = new ExclusiveAttribute(
            null,null,null,
            LockMethod.TRY,-1,TimeUnit.MILLISECONDS
    );

    private final ConcurrentHashMap<Object, ExclusiveAttribute> attributeCache = new ConcurrentHashMap<>(1024);

    @Nullable
    protected ExclusiveAttribute getExclusiveAttribute(Method method, @Nullable Class<?> targetClass) {
        if (method.getDeclaringClass() == Object.class) {
            return null;
        }

        Object cacheKey = getCacheKey(method, targetClass);
        ExclusiveAttribute cached = this.attributeCache.get(cacheKey);
        if (cached != null) {
            if (cached == NULL_EXCLUSIVE_ATTRIBUTE) {
                return null;
            }
            else {
                return cached;
            }
        }
        else {
            // We need to work it out.
            ExclusiveAttribute exAttr = computeTransactionAttribute(method, targetClass);
            // Put it in the cache.
            if (exAttr == null) {
                this.attributeCache.put(cacheKey, NULL_EXCLUSIVE_ATTRIBUTE);
            }
            else {
                String methodIdentification = ClassUtils.getQualifiedMethodName(method, targetClass);
                exAttr.setDescriptor(methodIdentification);
                if (logger.isTraceEnabled()) {
                    logger.trace("Adding exclusive method '" + methodIdentification + "' with attribute: " + exAttr);
                }
                this.attributeCache.put(cacheKey, exAttr);
            }
            return exAttr;
        }
    }
    protected Object getCacheKey(Method method, @Nullable Class<?> targetClass) {
        return new MethodClassKey(method, targetClass);
    }

    protected boolean allowPublicMethodsOnly() {
        return publicMethodsOnly;
    }

    @Nullable
    protected ExclusiveAttribute computeTransactionAttribute(Method method, @Nullable Class<?> targetClass) {
        // Don't allow no-public methods as required.
        if (allowPublicMethodsOnly() && !Modifier.isPublic(method.getModifiers())) {
            return null;
        }

        // The method may be on an interface, but we need attributes from the target class.
        // If the target class is null, the method will be unchanged.
        Method specificMethod = AopUtils.getMostSpecificMethod(method, targetClass);

        // First try is the method in the target class.
        ExclusiveAttribute txAttr = parseTransactionAnnotation(specificMethod);
        if (txAttr != null) {
            return txAttr;
        }

        // Second try is the transaction attribute on the target class.
        txAttr = parseTransactionAnnotation(specificMethod.getDeclaringClass());
        if (txAttr != null && ClassUtils.isUserLevelMethod(method)) {
            return txAttr;
        }

        if (specificMethod != method) {
            // Fallback is to look at the original method.
            txAttr = parseTransactionAnnotation(method);
            if (txAttr != null) {
                return txAttr;
            }
            // Last fallback is the class of the original method.
            txAttr = parseTransactionAnnotation(method.getDeclaringClass());
            if (txAttr != null && ClassUtils.isUserLevelMethod(method)) {
                return txAttr;
            }
        }

        return null;
    }


    @Nullable
    public ExclusiveAttribute parseTransactionAnnotation(AnnotatedElement element) {
        AnnotationAttributes attributes = AnnotatedElementUtils.findMergedAnnotationAttributes(
                element, Exclusive.class, false, false);
        if (attributes != null) {
            return parseTransactionAnnotation(attributes);
        }
        else {
            return null;
        }
    }
    public ExclusiveAttribute parseTransactionAnnotation(Exclusive ann) {
        return parseTransactionAnnotation(AnnotationUtils.getAnnotationAttributes(ann, false, false));
    }

    protected ExclusiveAttribute parseTransactionAnnotation(AnnotationAttributes attributes) {

        String lockName = attributes.getString("value");
        String exclusiveRepository = attributes.getString("exclusiveRepository");
        String messageWhenLockFailed = attributes.getString("messageWhenLockFailed");
        LockMethod lockMethod = attributes.getEnum("lockMethod");
        int timeout = attributes.getNumber("timeout");
        TimeUnit timeUnit = (TimeUnit)attributes.get("timeUnit");

        return new ExclusiveAttribute(
                lockName,exclusiveRepository,
                messageWhenLockFailed,lockMethod,
                timeout,timeUnit
        );
    }
}
